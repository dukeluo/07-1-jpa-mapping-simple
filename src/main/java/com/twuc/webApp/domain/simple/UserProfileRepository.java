package com.twuc.webApp.domain.simple;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserProfileRepository extends JpaRepository<UserProfile, Long>, CrudRepository<UserProfile, Long> {
    long deleteByName(String Name);
}
